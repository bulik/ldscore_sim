from __future__ import division
import numpy as np
import ldsc.jackknife as jk
import ldsc.gwas as gw
import ldsc.simulate as sim

# CC / CC Genetic correlation, optimal weights
ldScores = np.arange(4000)
M = int(np.sum(ldScores)); Meff = len(ldScores)
N_cas = np.array((2000,2000))
N_con = np.array((2000,2000))
N1 = N_cas[0]+N_con[0]; N2 = N_cas[1] + N_con[1] 
mafs = np.ones(Meff)/2
hsq1 = 0.7; hsq2 = 0.99; hsqs = np.array((hsq1, hsq2))
rho_g = -0.5; g_cor = rho_g / np.sqrt(hsq1*hsq2)
g_var = np.matrix(( (hsq1, rho_g ) ,  (rho_g, hsq2) )) 
e_var1 = 1 - hsq1; e_var2 = 1 - hsq2
rho_e = 0.25
e_var = np.matrix(( (e_var1,rho_e), (rho_e,e_var2) ))
rho = rho_e + rho_g
P = np.array((0.2, 0.3))
g_cor = rho_g / np.sqrt(hsq1*hsq2)
n_reps = 50
data = np.matrix(np.zeros((n_reps*2, 5)))
k = 0

for overlap in [0, 1000]:
	gc_weights = jk.gencor_weights(M, ldScores, N1, N2, overlap, hsq1, hsq2, rho_g, rho)
	h2_weights1 = jk.infinitesimal_weights(M, ldScores, N1*hsq1)
	h2_weights2 = jk.infinitesimal_weights(M, ldScores, N2*hsq2)
	for i in xrange(n_reps):
		print i
		### TODO replace with bivariatePointNormal
		beta = sim.rnorm(int(M), (0,0), g_var)
		beta_agg = sim.aggregateBeta(beta, ldScores)
		beta1 = beta_agg[:,0]; beta2 = beta_agg[:,1]
		x = gw.CC_CC_GWAS(Meff, N_cas, N_con, mafs, beta_agg, hsqs, P, e_var, overlap)
		y12 = jk.ldscore_reg(np.asarray(x.betadot).reshape((Meff,)), ldScores, weights=None, block_size=1000)
		y1 = jk.ldscore_reg(np.asarray(x.chisq1).reshape((Meff,)), ldScores, weights=h2_weights1, block_size=1000)
		y2 = jk.ldscore_reg(np.asarray(x.chisq2).reshape((Meff,)), ldScores, weights=h2_weights2, block_size=1000)
		# do something with the results
		data[k,0:4] = np.matrix((overlap, y12.est[0,0]*M, y1.est[0,0]*M/N1, y2.est[0,0]*M/N2))
		k += 1

data[:,4] = data[:,1] / np.sqrt(np.asarray(data[:,2])*np.asarray(data[:,3]) )
